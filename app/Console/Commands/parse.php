<?php

namespace App\Console\Commands;

use App\Models\Poster;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client([
            'verify'=>false,
        ]);
        $res = $client->request('GET', 'https://back-poster.admlr.lipetsk.ru/api/posters/?per_page=100');
        $code = $res->getStatusCode();
        $text = json_decode($res->getBody()->getContents());
        $data= $text->results;

        foreach ($data as $row){
            Poster::firstOrcreate(['id'=>$row->id,]
            ,
            [
                'id'=>$row->id,
                'title'=>$row->title,
                'image'=>$row->image,
                'price'=>$row->price,
                'date'=>json_encode($row->date),
                'address'=>$row->address,
                'comments_quantity'=>$row->comments_quantity,
                'likes_quantity'=>$row->likes_quantity,
                'is_liked'=>$row->is_liked,
            ]);
        };
        $id=1;
        for($det=124;$det<165;$det++){
            $detail = $client->request('GET', "https://back-poster.admlr.lipetsk.ru/api/posters/$det");
            $data_det = json_decode($detail->getBody()->getContents());
            Poster::findOrFail($id)->fill([
                'description'=>$data_det->description,
                'site'=>$data_det->site,
                'phones'=>$data_det->phones,
                'latitude'=>$data_det->latitude,
                'latitude'=>$data_det->latitude,
                'cat_id'=>$data_det->categories->id,
            ])->save();
        $id=$id+1;
        }
    }
}
