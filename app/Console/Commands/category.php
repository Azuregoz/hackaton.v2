<?php

namespace App\Console\Commands;

use App\Models\Category as ModelsCategory;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class category extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:cat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client([
            'verify'=>false,
        ]);
        $res = $client->request('GET', 'https://back-poster.admlr.lipetsk.ru/api/categories/');
        $code = $res->getStatusCode();
        $text = json_decode($res->getBody()->getContents());

        foreach ($text as $row){
            ModelsCategory::firstOrcreate(['id'=>$row->id,]
            ,[
                'id'=>$row->id,
                'title'=>$row->title,
                'image'=>$row->image,
                'description'=>$row->description,
            ]);
        };
    }
}
