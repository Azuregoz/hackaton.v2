<?php

namespace App\Console;
use App\Models\Poster;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use GuzzleHttp\Client;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $client = new Client([
                'verify'=>false,
            ]);
            $res = $client->request('GET', 'https://back-poster.admlr.lipetsk.ru/api/posters');
            $code = $res->getStatusCode();
            $text = json_decode($res->getBody()->getContents());
            $data= $text->results;

            foreach ($data as $row){
                Poster::firstOrcreate(['id'=>$row->id,]
                ,
                [
                    'id'=>$row->id,
                    'title'=>$row->title,
                    'image'=>$row->image,
                    'price'=>$row->price,
                    'date'=>json_encode($row->date),
                    'address'=>$row->address,
                    'comments_quantity'=>$row->comments_quantity,
                    'likes_quantity'=>$row->likes_quantity,
                    'is_liked'=>$row->is_liked,
                ]);
            };

        })->everyTwoHours();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
