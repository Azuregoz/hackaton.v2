<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PosterRecomendation extends Model
{
    use SoftDeletes;

    protected $table = "poster_recomendations";

    public $fillable=[
        'image',
        'title',
    ];
}
