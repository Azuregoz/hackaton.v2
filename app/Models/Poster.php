<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Poster extends Model
{
    use SoftDeletes;

    protected $table = "posters";

    public $fillable=[
        'title',
        'image',
        'price',
        'site',
        'date',
        'description',
        'address',
        'phones',
        'latitude',
        'longitude',
        'comments_quantity',
        'likes_quantity',
        'is_liked',
        'cat_id',
    ];

    public function category() {
        return $this->hasMany('App\Models\Category', 'id');
    }
    public function gallery() {
        return $this->hasOne('App\Models\GalleryPoster', 'id');
    }
    public function users(){
        return $this->belongsToMany(User::class,'favorites');
    }

    public function getDateAttribute($value) {
        $dates = json_decode($value);
        if ($dates) {
            return [
                'lower' => date("d.m.Y H:i", strtotime($dates->lower)),
                'upper' => date("d.m.Y H:i", strtotime($dates->upper)),
            ];
        }
        return null;
    }
}
