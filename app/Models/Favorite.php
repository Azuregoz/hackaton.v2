<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model
{
    use SoftDeletes;

    protected $table = "favorites";

    public $fillable=[
        'user_id',
        'poster_id',
    ];

    public function poster() {
        return $this->hasMany('App\Models\Poster', 'id','poster_id');
    }
}
