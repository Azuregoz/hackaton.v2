<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryPoster extends Model
{
    use SoftDeletes;

    protected $table = "gallery_posters";

    public $fillable=[
        'image',
    ];
}
