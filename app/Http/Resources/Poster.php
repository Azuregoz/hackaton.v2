<?php

namespace App\Http\Resources;

use App\Models\Poster as ModelsPoster;
use Illuminate\Http\Resources\Json\JsonResource;

class Poster extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->titlle,
            'image'=>$this->image,
            'price'=>$this->price,
            'site'=>$this->site,
            'date'=>$this->date,
            'description'=>$this->description,
            'address'=>$this->address,
            'phones'=>$this->phones,
            'latitude'=>$this->latitude,
            'longitude'=>$this->longitude,
            'categories'=>Category::collection($this->category),
            'comments_quantity'=>$this->comments_quantity,
            'recommendation'=>[ModelsPoster::select('id','image','title')
                                            ->wherebetween('id',[$this->id+1,$this->id+3])
                                            ->get(),],
            'is_liked'=>$this->is_liked,
        ];
    }
}
