<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Favorite extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'poster_id'=>$this->poster_id,
            'user_id'=>$this->user_id,
            'poster'=>PosterFull::collection($this->poster),
        ];
    }
}
