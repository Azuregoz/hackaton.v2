<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class PosterFull extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'cat_id'=>$this->cat_id,
            'title'=>$this->title,
            'image'=>$this->image,
            'price'=>$this->price,
            'address'=>$this->address,
            'date'=> $this->date,
            'is_liked'=>$this->is_liked,
        ];
    }
}
