<?php

namespace App\Http\Controllers;

use App\Http\Resources\Favorite as ResourcesFavorite;
use Illuminate\Http\Request;
use App\Http\Resources\Poster as ResourcesPoster;
use App\Http\Resources\PosterFull;
use App\Models\Category;
use App\Models\Favorite;
use App\Models\Poster;
use Illuminate\Http\Response;
use App\Models\User;

class PosterController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posters= new Poster();

        if ($request->query('sortPriceDesc')==true){
            $posters=$posters->orderByDesc('price');
    }
        if ($request->query('sortPrice')==true){
            $posters=$posters->orderBy('price');
    }
        if($filter=$request->query('filter')){
            $posters=$posters->whereCatId($filter);
        }
        if ($take=$request->take){
            $posters=$posters->take($take);
        }
        if ($skip=$request->skip){
            $posters=$posters->skip($skip);
        }
        $posters=$posters->get();

        if ($request->query('sortDate')==true) {
            return collect(PosterFull::collection($posters))->sortBy(function ($poster, $key) {
                return ($poster['date'] ? date("Y-m-d H:s", strtotime($poster['date']['lower'])) : $poster['id']);
            })->values()->all();
        }

        return PosterFull::collection($posters);
    }

    public function favorite(Request $request){
        $favorite = ResourcesFavorite::collection(Favorite::whereUserId($request->user_id)->get());
        return $favorite;
    }

    public function categories($id){
        return  Category::where('id','=',$id)->select('id','title')->get();
    }

    public function favoriteDel(Request $request){
        $fav_del= Favorite::wherePosterId($request->poster_id)->whereUserId($request->user_id)->firstOrFail();
        $fav_del->delete();

      return response()->json('', Response::HTTP_OK);
    }

    public function addFavorite(Request $request){
        Favorite::firstOrCreate([
            'user_id'=>$request->user_id,
            'poster_id'=>$request->poster_id,],
            [
                'user_id'=>$request->user_id,
                'poster_id'=>$request->poster_id,
                ]);

        return response()->json('', Response::HTTP_OK);
    }

    public function filtred(Request $request)
    {
        $poster = new Poster();
        $category = new Category();

        $temp=[
            '1'=>PosterFull::collection($poster->whereCatId('1')->get()),
            '2'=>PosterFull::collection($poster->whereCatId('2')->get()),
            '3'=>PosterFull::collection($poster->whereCatId('3')->get()),
            '4'=>PosterFull::collection($poster->whereCatId('4')->get()),
            '5'=>PosterFull::collection($poster->whereCatId('5')->get()),
            '6'=>PosterFull::collection($poster->whereCatId('6')->get()),
            'category'=>$category->select('id','title')->get(),
        ];
        return $temp;

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ResourcesPoster(Poster::FindOrFail($id));

    }
}
