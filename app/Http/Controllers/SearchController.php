<?php

namespace App\Http\Controllers;

use App\Http\Resources\PosterFull;
use Illuminate\Http\Request;
use App\Models\Poster;

class SearchController extends Controller
{
    public function search(Request $request){
        $poster = new Poster();
        if ($key=$request->query('key')){
            $poster=$poster->where(function($query) use ($key){
                $query->where('title', 'like','%'. $key.'%')
                ->orWhere('address', 'like','%'. $key.'%');
            });
        }
        return PosterFull::collection($poster->get());
    }
}
