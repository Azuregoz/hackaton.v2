<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    public function login(Request $request){
        $user = User::whereEmail($request->email)->firstOrFail();
        if (Hash::check($request->password, $user->password)) {
            $token =Str::random(60);
            $user->token=$token;
            $user->save();
            return json_encode([
                "token"=>$token,
                "name"=>$user->name,
                "id"=>$user->id,
            ]);
        }
        return response()->json('', Response::HTTP_NOT_FOUND);
    }

    public function logout(Request $request){
        if ($token=$request->header('token')){
            $user = User::whereToken($token)->firstOrFail();
            $user->token=null;
            $user->save();
            return response()->json('', Response::HTTP_OK);
        }
    }

    public function registration(Request $request){
        if($user = User::whereEmail('email','<>',$request->email)){
            $user->create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            ]);
            return response()->json('', Response::HTTP_OK);
        }
        return response()->json('', Response::HTTP_NOT_FOUND);
    }
}
