<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posters', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('cat_id')->nullable(true);
            $table->foreign('cat_id')->references('id')->on('category')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->string('title',255)->nullable(true);
            $table->string('image',255)->nullable(true);
            $table->integer('price')->nullable(true);
            $table->string('site')->nullable(true);
            $table->string('date',255)->nullable(true);
            $table->text('description')->nullable(true);
            $table->text('address')->nullable(true);
            $table->string('phones',255)->nullable(true);
            $table->float('latitude')->nullable(true);
            $table->float('longitude')->nullable(true);
            $table->integer('comments_quantity')->nullable(true);
            $table->integer('likes_quantity')->nullable(true);
            $table->boolean('is_liked')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posters');
    }
}
