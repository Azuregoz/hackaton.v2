<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleryPostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_posters', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('poster_id')->nullable(true);
            $table->foreign('poster_id')->references('id')->on('posters')
            ->onUpdate('cascade')
            ->onDelete('restrict');
            $table->string('image',255);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_posters');
    }
}
