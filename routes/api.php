<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::apiResource('poster', PosterController::class)->only(['index', 'show']);
    Route::get('poster_main', 'PosterController@filtred');
    Route::get('search', 'SearchController@search');
    Route::post('logout', 'AuthController@logout');
    Route::post('login', 'AuthController@login');
    Route::post('registration', 'AuthController@registration');
    Route::post('add_favorite','PosterController@addFavorite');
    Route::get('favorite','PosterController@favorite');
    Route::delete('del_favorite','PosterController@favoriteDel');
    Route::get('category/{id}','PosterController@categories');
});
